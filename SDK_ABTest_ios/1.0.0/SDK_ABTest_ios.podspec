Pod::Spec.new do |s|
  s.name = "SDK_ABTest_ios"
  s.version = "1.0.0"
  s.summary = "SDK_ABTest_ios 第一版测试"
  s.license = {"type"=>"MIT", "file"=>"LICENSE"}
  s.authors = {"Git.Z"=>"7280423@qq.com"}
  s.homepage = "https://gitlab.com/qil_sdk/sdk_specs"
  s.description = "TODO: Add long description of the pod here."
  s.source = { :http => 'https://raw.githubusercontent.com/7280423/QIL/master/SDK_ABTest_ios/1.0.0/SDK_ABTest_ios.zip' }

  s.ios.deployment_target    = '8.0'
  s.ios.vendored_framework   = 'ios/SDK_ABTest_ios.framework'
end
